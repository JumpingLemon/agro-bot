import re

class Matcher():
    def __init__(self, banlist, whitelist):
        self.banlist = banlist
        self.whitelist = whitelist

    def test(self, name):
        for ban in self.banlist:
            if ban in name:
                
                if name in self.whitelist:
                    return False
                return True

            maxScore = len(ban)

            for idx, sLetter in enumerate(ban):
                
                for inst in re.finditer(sLetter, name):
                    pos = inst.start()
                    score = 0

                    for idx2, letter in enumerate(ban[idx:]):
                        if idx2 >= len(name):
                            break;

                        if letter == name[idx2]:
                            score += 1
                        
                            if score > (maxScore/2):
                                
                                print(name, "achieved score of", score, "out of", maxScore/2)

                                if name in self.whitelist:
                                    return False
                                return True
        return False    
