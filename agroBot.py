# bot.py
import os # for importing env vars for the bot to use
from dotenv import load_dotenv

import atexit

from twitchio.ext import commands
from matcher import Matcher

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import pathlib
import json

import threading

load_dotenv()

baseBlacklist = [
    "hoss00",
    "corn_hub"
]

baseWhitelist = [
    "hoss00312",
]

settings = {
    "whitelist" : baseWhitelist,
    "blacklist" : baseBlacklist,
}

filePath = "settings.json"
pfile = pathlib.Path(filePath)
if pfile.exists() == False:
    with pfile.open('w') as f:
        f.write(json.dumps(settings, indent=4))

settings = json.loads(pfile.read_text())

print("using settings", settings)

class agroBot(commands.Bot):
    def __init__(self):
        super().__init__(
            token=os.environ['TMI_TOKEN'], 
            prefix=os.environ['BOT_PREFIX'], 
            initial_channels=[os.environ['CHANNEL']],
            nick=os.environ['BOT_NICK'])

        self.matcher = Matcher(settings["blacklist"], settings["whitelist"])

    async def event_ready(self):
        print("Sucessfully logged in as " + self.nick)

    async def testUser(self, user):
        ret = await user.user()
        print(ret)
        print("checking user", ret.name)
        result = self.matcher.test(ret.name)

        if result == True:
            # await user.UserBan()
            await self.get_channel(os.environ['CHANNEL']).send("/ban " + str(ret.name))

            print("User", ret, "was deemed a bot and banned")

    async def event_join(self, channel, user):
        await self.testUser(user)

    async def event_part(self, user):
        await self.testUser(user)

    async def event_message(self, message):
        user = message.author
        await self.testUser(user)

    async def event_userstate(self, user):
        await self.testUser(user)

    async def event_raw_data(self, str):
        print(str)

    @commands.command()
    async def hello(self, ctx: commands.Context):
        await ctx.send("hiiiiii")

bot = agroBot()

class EventHandler(FileSystemEventHandler):
    def on_modified(self, event):
        global settings
        global pfile

        lpfile = pathlib.Path(event.src_path)

        if pfile.absolute().as_posix() == lpfile.absolute().as_posix():
            print("loading new lists")
            try:
                lock = threading.Lock()

                lock.acquire()
                settings = json.loads(pfile.read_text())
                bot.matcher.banlist = settings["blacklist"]
                bot.matcher.whitelist = settings["whitelist"]
                lock.release()
                print("lists loaded")

                
                print("using settings", settings)
            except:
                print("couldn't load file")

observer = Observer()
observer.schedule(EventHandler(), pfile.absolute().parent.as_posix() )
observer.start()


def onExit():
    print("exitting....")
    bot.close()

if __name__ == "__main__":
    atexit.register(onExit)
    bot.run()